# ReactCalc

A simple calculator written in react

Demo here: [React Calc](https://samuelts.com/ReactCalc)

### Images

![Calculator](https://raw.githubusercontent.com/safwyls/logos/master/Calculator.png)
