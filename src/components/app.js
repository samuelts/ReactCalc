import React, { Component } from 'react';
import Calculator from './calculator';

export default class App extends Component {
  render() {
    return (
      <div className='container'>
        <Calculator />
        <footer>
          Display Font courtesy of <a href="https://fontstruct.com/fontstructions/show/101159/spell_a">Lord Nightmare on fontstruct</a>
        </footer>
      </div>
    );
  }
}
