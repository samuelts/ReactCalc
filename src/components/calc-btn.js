import React, { Component } from 'react';

export default class CalcBtn extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.callCalc = this.callCalc.bind(this);
  }

  // Add event listeners on mount for keypress/release
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress.bind(this));
    document.addEventListener('keyup', this.handleKeyUp.bind(this));
  }

  // Remove event listeners on unmount for keypress/release
  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress.bind(this));
    document.removeEventListener('keyup', this.handleKeyUp.bind(this));
  }

  // Set button active state on keypress
  handleKeyPress(event) {
    if (event.key === this.props.keyCode) {
      document.getElementById(this.props.id).classList.add("btn-active");
      this.callCalc();
    }
  }

  // Remove button active state on key release
  handleKeyUp(event) {
    if (event.key === this.props.keyCode) {
      document.getElementById(this.props.id).classList.toggle("btn-active");
    }
  }

  // Handle click function (separated from callCalc function for semantic reasons)
  handleClick() {
    this.callCalc();
  }

  // Call callback function to prep audio for play
  callCalc() {
    this.props.appendVal(this.props.keyCode)
  }

  render() {
    return (
      <button
        className='calc-btn btn btn-secondary'
        onClick={this.handleClick.bind(this)}
        id={this.props.id}
      >
        {this.props.keyVal}
      </button>
    );
  }
}