import React, { Component } from 'react';
import Display from './display';
import CalcBtn from './calc-btn';

class Stack {
  constructor() {
    this.items = [];
  }

  push(elem) {
    this.items.push(elem);
  }

  pop() {
    if (this.items.length == 0) {
      return 'Underflow';
    }
    return this.items.pop();
  }

  peek() {
    return this.items[this.items.length - 1];
  }

  isEmpty() {
    const empty = this.items.length == 0 ? true : false;
    return empty;
  }

  printStack() {
    let str = '';
    this.items.map((elem) => str += " " + elem);
    return str;
  }
}

export default class Calculator extends Component {
  constructor(props) {
    super(props)

    this.state={
      disp: '0'
    };

    this.appendVal = this.appendVal.bind(this);
    this.infixToPostfix = this.infixToPostfix.bind(this);
    this.postfixEval = this.postfixEval.bind(this);
  }

  appendVal(value) {
    let disp = this.state.disp;

    switch (value) {
      case 'Enter':
        this.infixToPostfix();
        break;
      case 'Delete':
        this.setState({
          disp: '0'
        });
        break;
      case 'Backspace':
        if (disp.substring) {
          this.setState({
            disp: disp.substring(0, disp.length - 1)
          });
        } else {
          this.setState({
            disp: '0'
          });
        }
        break;
      case '0':
        // If display string is only zero dont add another zero
        if (disp === '0') {
          break;
        }
      case '.':
        // Grab the last number up to an operator in the display string
        let lastNum = disp.substring(disp.search(/([^*/+-]+)(?!.)/));
        // If that last number contains a decimal don't add another (5.5.5 => 5.55)
        if (/\./.test(lastNum) && value == '.') {
          break;
        }
      default:

        // Replace multiple consecutive operators with last operator
        let sane;
        if (/[*/+-]/.test(disp)) {
          sane = disp.replace(/[*/+-]{2,}/g, (match) => match.charAt(match.length-1));
        } else {
          sane = disp;
        }

        this.setState({
          disp: disp === '0' && value != '.' ? value : sane + value
        });
        break;
    }
  }

  infixToPostfix() {
    var opstack = new Stack();
    var postfix = [];
    const input = this.state.disp;
    const operator = /([*/+\-\(\)])/g;
    const nums = /\d+/;
    const prec = {
      '*': 3,
      '/': 3,
      '+': 2,
      '-': 2,
      '(': 1
    };

    var infix = input.split(operator).filter((elem) => elem != "");

    for (let i = 0; i < infix.length; i++) {
      let token = infix[i];
      if (nums.test(token)) {
        postfix.push(token);
      } else if (token == '(') {
        opstack.push(token);
      } else if (token == ')') {
        let topToken = opstack.pop();
        while (topToken != '(') {
          postfix.push(topToken);
          topToken = opstack.pop();
        }
      } else {
        while (!opstack.isEmpty() && prec[opstack.peek()] >= prec[token]) {
          postfix.push(opstack.pop());
        }
        opstack.push(token);
      }
    }

    while (!opstack.isEmpty()) {
      postfix.push(opstack.pop());
    }

    let temp = postfix.join(' ');
    this.setState({
      disp: temp
    });

    console.log('infix: ', infix);
    console.log('postfix: ', postfix);

    this.postfixEval(postfix);
  }

  postfixEval(postfix) {
    var evalstack = new Stack();
    const operator = /[*/+-]/;

    for (let i = 0; i < postfix.length; i++) {
      if (operator.test(postfix[i])) {
        const second = parseFloat(evalstack.pop());
        const first = parseFloat(evalstack.pop());
        const doMath = {
          '+': (x,y) => x + y,
          '-': (x,y) => x - y,
          '*': (x,y) => x * y,
          '/': (x,y) => x / y
        };
        let result = doMath[postfix[i]](first,second);
        evalstack.push(result);
      } else {
        evalstack.push(postfix[i]);
      }
    }

    this.setState({
      disp: evalstack.peek()
    });
    console.log(this.state.disp);
  }

  render() {
    const numRow0 = [
      {keyCode: '(',
        keyVal: '(',
        id: 'left_paren'},
      {keyCode: ')',
        keyVal: ')',
        id: 'right_paren'},
      {keyCode: 'Backspace',
        keyVal: 'C',
        id: 'percent'},
      {keyCode: 'Delete',
        keyVal: 'AC',
        id: 'clear'}];

    const numRow1 = [
      {keyCode: '7',
        keyVal: '7',
        id: 'seven'},
      {keyCode: '8',
        keyVal: '8',
        id: 'eight'},
      {keyCode: '9',
        keyVal: '9',
        id: 'nine'},
      {keyCode: '/',
        keyVal: <i class="fas fa-divide"></i>,
        id: 'divide'}];

    const numRow2 = [
      {keyCode: '4',
        keyVal: '4',
        id: 'four'},
      {keyCode: '5',
        keyVal: '5',
        id: 'five'},
      {keyCode: '6',
        keyVal: '6',
        id: 'six'},
      {keyCode: '*',
        keyVal: <i class="fas fa-times"></i>,
        id: 'multiply'}];

    const numRow3 = [
      {keyCode: '1',
        keyVal: '1',
        id: 'one'},
      {keyCode: '2',
        keyVal: '2',
        id: 'two'},
      {keyCode: '3',
        keyVal: '3',
        id: 'three'},
      {keyCode: '-',
        keyVal: <i class="fas fa-minus"></i>,
        id: 'subtract'}];

    const numRow4 = [
      {keyCode: '0',
        keyVal: '0',
        id: 'zero'},
      {keyCode: '.',
        keyVal: '.',
        id: 'decimal'},
      {keyCode: 'Enter',
        keyVal: <i class="fas fa-equals"></i>,
        id: 'equals'},
      {keyCode: '+',
        keyVal: <i class="fas fa-plus"></i>,
        id: 'add'}];

    const calcRow0 = numRow0.map((key) => {
      return (
        <CalcBtn
          keyCode={key.keyCode}
          keyVal={key.keyVal}
          id={key.id}
          appendVal={this.appendVal}
        />
      );
    });

    const calcRow1 = numRow1.map((key) => {
      return (
        <CalcBtn
          keyCode={key.keyCode}
          keyVal={key.keyVal}
          id={key.id}
          appendVal={this.appendVal}
        />
      );
    });

    const calcRow2 = numRow2.map((key) => {
      return (
        <CalcBtn
          keyCode={key.keyCode}
          keyVal={key.keyVal}
          id={key.id}
          appendVal={this.appendVal}
        />
      );
    });

    const calcRow3 = numRow3.map((key) => {
      return (
        <CalcBtn
          keyCode={key.keyCode}
          keyVal={key.keyVal}
          id={key.id}
          appendVal={this.appendVal}
        />
      );
    });

    const calcRow4 = numRow4.map((key) => {
      return (
        <CalcBtn
          keyCode={key.keyCode}
          keyVal={key.keyVal}
          id={key.id}
          appendVal={this.appendVal}
        />
      );
    });

    return (
      <div className='wrapper' id='calculator'>
        <div className='display-wrapper'>
          <Display disp={this.state.disp} />
        </div>
        <div className='btn-wrapper'>
          <div id='row-0'>
            {calcRow0}
          </div>
          <div id='row-1'>
            {calcRow1}
          </div>
          <div id='row-2'>
            {calcRow2}
          </div>
          <div id='row-3'>
            {calcRow3}
          </div>
          <div id='row-4'>
            {calcRow4}
          </div>
        </div>
      </div>
    );
  }
}