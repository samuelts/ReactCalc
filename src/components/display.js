import React from 'react';

const Display = (props) => {
    return (
      <textarea
        id='display'
        value={props.disp}
        readonly
      />
    );
}
export default Display;